//
//  Constants.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import UIKit

// All Constants

//MARK:- API Constants

let kBaseURL = "https://my-json-server.typicode.com/imkhan334/demo-1"
let kGetCallDataEndPoint = "/call"
let kGetToBuyDataEndPoint = "/buy"


// MARK:- Nib and Cell Identifiers
let kToCallCellNib = "ToCallTableViewCell"
let kToCallCellIdentifier = "ToCallCell"
let kToCallCellHeight = CGFloat(120)
let kItemCellNib = "ItemTableViewCell"
let kItemCellIdentifier = "ItemCell"
let kItemCellHeight = CGFloat(200)


// MARK:- Segues

let kShowItemsToSellSegue = "showToSellItemsSegue"
let kShowItemsToBuySeque = "showToBuyItemsSegue"

// MARK:- Database Constants

let kDBName = "jl-demo-db"
let kDBFileType = "db"
let kItemsToSellTableName = "ItemToSell"

// MARK:- Storyboard constants

let kMainStoryBoard = "Main"
let kHomeNavigationController = "HomeNavigationController"


// MARK:- Alert Constants

let kAlertTitle = "Alert"
let kNoInternetAlert = "Not connected to internet, please connect and try again"
let kUnableToReadDB = "Could not get data at the moment, please try again later"
let kDBNotFound = "Unable to connect to database, please try again later"
