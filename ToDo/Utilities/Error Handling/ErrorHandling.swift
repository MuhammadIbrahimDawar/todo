//
//  ErrorHandling.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation

enum ReachibilityError: Error
{
    case InternetNotAvailable
}

enum DBError: Error
{
    case UnableToReadDatabase
    case DBNotFound
}


extension ReachibilityError: LocalizedError
{
    public var errorDescription: String? {
        switch self {
        case .InternetNotAvailable:
            return kNoInternetAlert
        }
    }
}


extension DBError: LocalizedError
{
    public var errorDescription: String? {
        switch self {
        case .UnableToReadDatabase:
            return kUnableToReadDB
        case .DBNotFound:
            return kDBNotFound
        }
    }
}
