//
//  UILabelExtension.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import UIKit


extension UILabel
{
    func underLineText()
    {
        guard let text = self.text else {return}
        
        // getting text from label and adding underling to it using NSMutableAttributed string
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.textColor!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.textColor!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.attributedText = attributedString
       
    }
}
