//
//  NVActivityIndicatior.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable{
    
    // NVActivityIndicator setup to make all UIViewControllers have methods like startAnimating and stopAnimation for activity indicator
    public func configureIndicator(){
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor(red:0.99, green:0.82, blue:0.02, alpha:1)
        NVActivityIndicatorView.DEFAULT_TYPE = NVActivityIndicatorType.ballGridPulse
        NVActivityIndicatorView.DEFAULT_TYPE = .ballGridPulse
        startAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("")
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            
        }
        
        
    }
}
