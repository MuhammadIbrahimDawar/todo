//
//  DBManager.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import SQLite


class DBManager
{
    // DBManager to read from provided SQLite database
    var delegate: DBDelegate?
    
    func fetchItemsData()
    {
        var items: [Item]=[]
        let id = Expression<Int64>("id") // column "id"
        let name = Expression<String>("name") // column "name"
        let price = Expression<Double>("price") // column "price"
        let quantity = Expression<Int64>("quantity") // column "quantity"
        let type = Expression<Int64>("type") // column "type"
        
        // loading db with provided dbName and its filetype
        if let databaseFilePath = Bundle.main.path(forResource: kDBName, ofType: kDBFileType){
            
            let db = try! Connection(databaseFilePath)
            let toSellItems = Table(kItemsToSellTableName)
            
            for item in try! db.prepare(toSellItems) {
                // Converting each row to "Item" and appending in items array
                let newItem = ItemModelClass(id: item[id], name: item[name], price: item[price], quantity: item[quantity], type: item[type])
                
                items.append(newItem)
            }
            // success delegate method fired
            self.delegate?.didFetchData(self, with: items)
        }
        else {
            // failure delegate method fired
            delegate?.didFailWithError(self, with: DBError.DBNotFound)
        }
    }
    
    
    
}
