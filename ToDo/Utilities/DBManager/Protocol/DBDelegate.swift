//
//  DBDelegate.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation

// DBDelegate to pass DB events
protocol DBDelegate
{
    func didFetchData(_ dbManager: DBManager, with items: [Item])
    func didFailWithError(_ dbManager: DBManager, with error: DBError)
}
