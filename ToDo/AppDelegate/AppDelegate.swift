//
//  AppDelegate.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/22/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    class func sharedInstance ()-> AppDelegate?{
        let sharedInstance = {
            UIApplication.shared.delegate as! AppDelegate
        }()
        return sharedInstance;
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        configureHomeViewController()
        return true
    }

}



extension AppDelegate
{
    func showAlert(title: String, message: String)
    {
        let myAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        myAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.window?.rootViewController?.present(myAlert, animated: true, completion: nil)
        
    }
    
    func configureHomeViewController()
    {
        self.window = UIWindow()
        let storyboard = UIStoryboard(name: kMainStoryBoard, bundle: nil)
        let homeNavigationController = storyboard.instantiateViewController(withIdentifier: kHomeNavigationController)
        self.window?.rootViewController = homeNavigationController
    }
}
