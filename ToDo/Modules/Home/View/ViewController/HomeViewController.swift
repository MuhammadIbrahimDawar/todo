//
//  ViewController.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/22/20.
//

import UIKit

class HomeViewController: UIViewController {

    private var homeViewModel: HomeViewModel!
    
    @IBOutlet weak var toCallView: UIView!
    
    @IBOutlet weak var toBuyView: UIView!
    
    @IBOutlet weak var toSellView: UIView!
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureHomeViewModel()
    }
    
    private func configureView()
    {
        // rouding corners and giving shadow to button views
        toCallView.roundCorners(with: 5)
        toCallView.dropShadow()
        toBuyView.roundCorners(with: 5)
        toBuyView.dropShadow()
        toSellView.roundCorners(with: 5)
        toSellView.dropShadow()
    }
    
    private func configureHomeViewModel()
    {
        // view model initialisation
        homeViewModel = HomeViewModel()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == kShowItemsToBuySeque
        {
            // segue to show to buy items
            if let destinationVC = segue.destination as? BuyAndSellItemViewController
            {
                destinationVC.itemsType = .ToBuy
            }
        } // segue to show to sell items
        else if segue.identifier == kShowItemsToSellSegue
        {
            if let destinationVC = segue.destination as? BuyAndSellItemViewController
            {
                destinationVC.itemsType = .ToSell
            }
        }
    }
}

