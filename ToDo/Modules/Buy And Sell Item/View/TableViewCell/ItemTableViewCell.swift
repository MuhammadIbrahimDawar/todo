//
//  ItemTableViewCell.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var quantityTitleLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBOutlet weak var typeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    private func configureView()
    {
        // configuring main view with corner radius and drop down shadow
        mainView.roundCorners(with: 5)
        mainView.dropShadow()
        // add an underline to labels as in provided design
        nameTitleLabel.underLineText()
        priceTitleLabel.underLineText()
        quantityTitleLabel.underLineText()
    }

    override func prepareForReuse()
    {
        // removing string data from labels before reuse
        nameLabel.text = ""
        priceLabel.text = ""
        quantityLabel.text = ""
        typeLabel.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configure(with item: Item)
    {
        //configuring cell view with provided item
        nameLabel.text = item.itemName
        priceLabel.text = item.itemPrice
        quantityLabel.text = item.itemQuantity
        typeLabel.text = item.itemType.getTypeString()
    }
}
