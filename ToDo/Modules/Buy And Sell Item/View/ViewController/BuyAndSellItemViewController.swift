//
//  BuyAndSellItemViewController.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import UIKit



class BuyAndSellItemViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var buyAndSellViewModel: BuyAndSellItemViewModel!
    
    var itemsType = ItemType.ToBuy
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewModel()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // setting navigation item title to "To Buy" or "To Sell" accordingly
        navigationItem.title = itemsType.getTypeString()
    }
    
    
    private func configureViewModel()
    {
        //start animating activity indicator for better user experience
        startAnimating()
        // configuring view model
        buyAndSellViewModel = BuyAndSellItemViewModel(itemsType: itemsType)
        
        // avoiding retain cycle
        weak var weakSelf = self
        
        // view model success call back
        buyAndSellViewModel.didReceiveItemsData = {
            weakSelf?.stopAnimating()
            weakSelf?.tableView.reloadData()
        }
        
        // view model failure call back
        buyAndSellViewModel.didRecieveError = { errorMessage in
            weakSelf?.stopAnimating()
            AppDelegate.sharedInstance()?.showAlert(title: kAlertTitle, message: errorMessage)
        }
        
        buyAndSellViewModel.getItemsData()
    }
    
    private func configureTableView()
    {
        // tableView configuration, nib registration, delegate, and datasource
        tableView.register(UINib(nibName: kItemCellNib, bundle: nil), forCellReuseIdentifier: kItemCellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
    }

}

// MARK:- TableView Delegate & DataSource
extension BuyAndSellItemViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buyAndSellViewModel.itemsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: kItemCellIdentifier, for: indexPath) as? ItemTableViewCell
        else{
            return UITableViewCell()
        }
        
        if let item = buyAndSellViewModel.itemsData?[indexPath.row]
        {
            cell.configure(with: item)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return kItemCellHeight
    }
    
    
}
