//
//  Item.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation

enum ItemType: Int
{
    case ToBuy = 1
    case ToSell = 2
    case NotIdentified = 3
    
    func getTypeString() -> String
    {
        switch self.rawValue
        {
        case 1:
            return "To Buy"
        case 2:
            return "To Sell"
        default:
            return "Not Identified"
        }
    }
}

// Protocol for dependency injection
protocol Item
{
    var itemName: String {get}
    var itemPrice: String {get}
    var itemQuantity: String {get}
    var itemType: ItemType {get}
    
    init(id: Int64, name: String, price: Double, quantity: Int64, type: Int64) 
}
