//
//  ItemResponseBaseClass.swift
//
//  Created by Muhammad Ibrahim on 12/23/20
//  Copyright (c) Joblogic. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ItemModelClass: NSObject, NSCoding, Item {
    
    // conformed to protocol item for dependency injection
    
    var itemName: String
    {
        return name ?? ""
    }
    
    var itemPrice: String
    {
        return price != nil ? "\(price!)" : ""
    }
    
    var itemQuantity: String {
        return quantity != nil ? "\(quantity!)" : ""
    }
    
    var itemType: ItemType {
        
        if let type = ItemType(rawValue: Int(type ?? 1))
        {
            return type
        }
        return ItemType.NotIdentified
    }
    
    
    public func encode(with coder: NSCoder) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kItemResponseBaseClassPriceKey: String = "price"
	internal let kItemResponseBaseClassNameKey: String = "name"
	internal let kItemResponseBaseClassQuantityKey: String = "quantity"
	internal let kItemResponseBaseClassInternalIdentifierKey: String = "id"
	internal let kItemResponseBaseClassTypeKey: String = "type"


    // MARK: Properties
	public var price: Double?
	public var name: String?
	public var quantity: Int64?
	public var internalIdentifier: Int64?
	public var type: Int64?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    required init(id: Int64, name: String, price: Double, quantity: Int64, type: Int64) {
        self.internalIdentifier = id
        self.price = price
        self.name = name
        self.quantity = quantity
        self.type = type
    }
    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		price = json[kItemResponseBaseClassPriceKey].double
		name = json[kItemResponseBaseClassNameKey].string
		quantity = json[kItemResponseBaseClassQuantityKey].int64
		internalIdentifier = json[kItemResponseBaseClassInternalIdentifierKey].int64
		type = json[kItemResponseBaseClassTypeKey].int64

    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : Any ] {

        var dictionary: [String : Any ] = [ : ]
		if price != nil {
			dictionary.updateValue(price!, forKey: kItemResponseBaseClassPriceKey)
		}
		if name != nil {
			dictionary.updateValue(name!, forKey: kItemResponseBaseClassNameKey)
		}
		if quantity != nil {
			dictionary.updateValue(quantity!, forKey: kItemResponseBaseClassQuantityKey)
		}
		if internalIdentifier != nil {
			dictionary.updateValue(internalIdentifier!, forKey: kItemResponseBaseClassInternalIdentifierKey)
		}
		if type != nil {
			dictionary.updateValue(type!, forKey: kItemResponseBaseClassTypeKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.price = aDecoder.decodeObject(forKey: kItemResponseBaseClassPriceKey) as? Double
        self.name = aDecoder.decodeObject(forKey: kItemResponseBaseClassNameKey) as? String
        self.quantity = aDecoder.decodeObject(forKey: kItemResponseBaseClassQuantityKey) as? Int64
        self.internalIdentifier = aDecoder.decodeObject(forKey: kItemResponseBaseClassInternalIdentifierKey) as? Int64
        self.type = aDecoder.decodeObject(forKey: kItemResponseBaseClassTypeKey) as? Int64

    }

    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encode(price, forKey: kItemResponseBaseClassPriceKey)
        aCoder.encode(name, forKey: kItemResponseBaseClassNameKey)
        aCoder.encode(quantity, forKey: kItemResponseBaseClassQuantityKey)
        aCoder.encode(internalIdentifier, forKey: kItemResponseBaseClassInternalIdentifierKey)
        aCoder.encode(type, forKey: kItemResponseBaseClassTypeKey)

    }

}
