//
//  BuyAndSellItemViewModel.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import SwiftyJSON

class BuyAndSellItemViewModel
{
    // Model array
    var itemsData: [Item]?
    // success call back method
    var didReceiveItemsData: (() -> Void)?
    // failure call back method
    var didRecieveError: ((_ errorMessage: String) -> Void)?
    // network operation object
    private let apiOperation: NetworkOperation
    // items type enum
    private var itemsType: ItemType
    // DB manager
    private var dbManager: DBManager
    
    init(itemsType: ItemType)
    {
        self.apiOperation = NetworkOperation()
        self.dbManager = DBManager()
        self.itemsType = itemsType
    }
    
    
    func getItemsData()
    {
        // if items belong to "To Buy" type network request is made else DB query is made
        if itemsType == .ToBuy
        {
            requestApiToFetchData()
        }
        else if itemsType == .ToSell
        {
            getDataFromLocalDB()
        }
    
    }
    
    private func requestApiToFetchData()
    {
        // avoiding retain cycle
        unowned let weakSelf = self
        
        
        
        apiOperation.didRecieveSuccessResponse = { response in
            
            guard response != nil else{
                return
            }
            
            // converting JSON to JSON array
            let jsonData = JSON(response!).array
            
            if jsonData != nil
            {
                weakSelf.itemsData = [Item]()
                // converting each JSON objec to model object
                for data in jsonData!
                {
                    weakSelf.itemsData!.append(ItemModelClass(json: data))
                }
                // success call back method fired
                weakSelf.didReceiveItemsData?()
            }
        }
        // failure call back method fired
        apiOperation.didRecieveFailureResponse = { error in
            if let reachibilityError = error as? ReachibilityError
            {
                weakSelf.didRecieveError?(reachibilityError.errorDescription ?? "")
            }
            else{
                weakSelf.didRecieveError?(error.localizedDescription)
            }
        }
        
        apiOperation.requestApi(with: kGetToBuyDataEndPoint, method: .get, having: nil)
    }
    
    private func getDataFromLocalDB()
    {
        dbManager.delegate = self
        dbManager.fetchItemsData()
    }
    
    
}
// conforming DBDelegate
extension BuyAndSellItemViewModel: DBDelegate
{
    func didFetchData(_ dbManager: DBManager, with items: [Item]) {
        self.itemsData = items
        DispatchQueue.main.async { [unowned self] in
            self.didReceiveItemsData?()
        }
    }
    
    func didFailWithError(_ dbManager: DBManager, with error: DBError) {
        DispatchQueue.main.async { [unowned self] in
            self.didRecieveError?(error.errorDescription ?? "")
        }
       
    }
    
    
}
