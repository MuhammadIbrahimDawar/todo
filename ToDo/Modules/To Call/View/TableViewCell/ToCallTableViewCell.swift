//
//  ToCallTableViewCell.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import UIKit

class ToCallTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureView()
    }
    
    private func configureView()
    {
        mainView.dropShadow()
        mainView.roundCorners(with: 5)
    }
    
    override func prepareForReuse() {
        nameLabel.text = ""
        numberLabel.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with data: ToCallModelClass)
    {
        nameLabel.text = data.name
        numberLabel.text = data.number
    }
    
}
