//
//  ToCallViewController.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import UIKit

class ToCallViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    private var toCallViewModel: ToCallViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewModel()
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    
    
    private func configureViewModel()
    {
        startAnimating()
        toCallViewModel = ToCallViewModel()

        toCallViewModel.didReceiveCallData = { [weak self] in
            self?.stopAnimating()
            self?.tableView.reloadData()
        }
        
        toCallViewModel.didRecieveError = { [weak self] errorMessage in
            self?.stopAnimating()
            AppDelegate.sharedInstance()?.showAlert(title: kAlertTitle, message: errorMessage)
        }
        
        toCallViewModel.getCallData()
    }
    
    private func configureTableView()
    {
        tableView.register(UINib(nibName: kToCallCellNib, bundle: nil), forCellReuseIdentifier: kToCallCellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
    }

}

// MARK:- TableView Delegate & DataSource
extension ToCallViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toCallViewModel.callData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: kToCallCellIdentifier, for: indexPath) as? ToCallTableViewCell
        else{
            return UITableViewCell()
        }
        
        if let callData = toCallViewModel.callData?[indexPath.row]
        {
            cell.configure(with: callData)
        }
    
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return kToCallCellHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
