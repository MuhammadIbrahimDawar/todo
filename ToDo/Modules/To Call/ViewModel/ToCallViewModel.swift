//
//  ToCallViewModel.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import SwiftyJSON


class ToCallViewModel
{
    // network opration object
    private let apiOperation: NetworkOperation
    // model array
    var callData: [ToCallModelClass]?
    //success call back method
    var didReceiveCallData: (() -> Void)?
    // failure call back method
    var didRecieveError: ((_ errorMessage: String) -> Void)?
    
    
    init() {
        apiOperation = NetworkOperation()
    }
    
    
    func getCallData()
    {
        // weakSelf to avoid retain cycles
        unowned let weakSelf = self
        
        apiOperation.didRecieveSuccessResponse = { response in
            
            guard response != nil else{
                return
            }
            
            // converting fetched JSON object to JSON array
            let jsonData = JSON(response!).array
            
            if jsonData != nil
            {
                weakSelf.callData = [ToCallModelClass]()
                
                for data in jsonData!
                {
                    //converting each object in JSON array to model class object and appending in model array
                    weakSelf.callData!.append(ToCallModelClass(json: data))
                }
             
                weakSelf.didReceiveCallData?()
            }
        }
        
        apiOperation.didRecieveFailureResponse = { error in
            
            if let reachibilityError = error as? ReachibilityError
            {
                weakSelf.didRecieveError?(reachibilityError.errorDescription ?? "")
            }
            else{
                weakSelf.didRecieveError?(error.localizedDescription)
            }
            
        }
    
        apiOperation.requestApi(with: kGetCallDataEndPoint, method: .get, having: nil)
    }
    
    
}
