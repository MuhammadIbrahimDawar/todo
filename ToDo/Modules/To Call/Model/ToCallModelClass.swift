//
//  CallResponseBaseClass.swift
//
//  Created by Muhammad Ibrahim on 12/23/20
//  Copyright (c) Joblogic. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ToCallModelClass: NSObject, NSCoding {
    public func encode(with coder: NSCoder) {
        
    }
    

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kCallResponseBaseClassInternalIdentifierKey: String = "id"
	internal let kCallResponseBaseClassNumberKey: String = "number"
	internal let kCallResponseBaseClassNameKey: String = "name"


    // MARK: Properties
	public var internalIdentifier: Int?
	public var number: String?
	public var name: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		internalIdentifier = json[kCallResponseBaseClassInternalIdentifierKey].int
		number = json[kCallResponseBaseClassNumberKey].string
		name = json[kCallResponseBaseClassNameKey].string

    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : Any ] {

        var dictionary: [String : Any ] = [ : ]
		if internalIdentifier != nil {
			dictionary.updateValue(internalIdentifier!, forKey: kCallResponseBaseClassInternalIdentifierKey)
		}
		if number != nil {
			dictionary.updateValue(number!, forKey: kCallResponseBaseClassNumberKey)
		}
		if name != nil {
			dictionary.updateValue(name!, forKey: kCallResponseBaseClassNameKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.internalIdentifier = aDecoder.decodeObject(forKey: kCallResponseBaseClassInternalIdentifierKey) as? Int
        self.number = aDecoder.decodeObject(forKey: kCallResponseBaseClassNumberKey) as? String
        self.name = aDecoder.decodeObject(forKey: kCallResponseBaseClassNameKey) as? String

    }

    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encode(internalIdentifier, forKey: kCallResponseBaseClassInternalIdentifierKey)
        aCoder.encode(number, forKey: kCallResponseBaseClassNumberKey)
        aCoder.encode(name, forKey: kCallResponseBaseClassNameKey)

    }

}
