//
//  NetworkOperation.swift
//  ToDo
//
//  Created by Muhammad Ibrahim on 12/23/20.
//

import Foundation
import Alamofire
import SwiftyJSON
import Reachability

class NetworkOperation
{
    // success call back method
    var didRecieveSuccessResponse: ((_ response: AnyObject?) -> Void)?
    // failure call back method
    var didRecieveFailureResponse: ((_ error: Error) -> Void)?
    
    // network call to request data from backend
    func requestApi(with endpoint: String, method type: HTTPMethod, having parameters: [String:Any]?)
    {
        do{
            // checking internet Connection
            if try Reachability().connection == .unavailable
            {
                
                didRecieveFailureResponse?(ReachibilityError.InternetNotAvailable)
                return
            }
        }
        catch
        {
            didRecieveFailureResponse?(error)
            return
        }
       
        weak var weakSelf = self
        
        let url = kBaseURL + endpoint
        // Alamofire request
        AF.request(url, method: type, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch (response.result) {
            case .success:
                weakSelf?.didRecieveSuccessResponse?(response.data as AnyObject?)
                break
            case .failure:
                guard case let .failure(error) = response.result else { return }
                weakSelf?.didRecieveFailureResponse?(error)
            }
        }
    }
}
